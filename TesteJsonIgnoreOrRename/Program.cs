﻿using System;
using Newtonsoft.Json;
using TesteJsonIgnoreOrRename.NewtonsoftConfiguration.Configurations;

namespace TesteJsonIgnoreOrRename
{
    class Program
    {
        static void Main(string[] args)
        {
            var pessoa = GetPessoaTeste();

            // normal
            Console.WriteLine(JsonConvert.SerializeObject(pessoa));

            // Adicionando para ignorar campos
            var pessoaJsonSettings = new JsonSettings();
            pessoaJsonSettings.IgnoreProperties<Pessoa>("Nome", "Sobrenome", "DataNascimento");
            pessoaJsonSettings.RenameProperties<Pessoa>("Id", "PessoaId");
            Console.WriteLine(JsonConvert.SerializeObject(pessoa, pessoaJsonSettings.GetJsonSerializerSettings()));

            Console.WriteLine(args);

            Console.ReadKey();
        }
        
        private static Pessoa GetPessoaTeste()
        {
            return new Pessoa()
            {
                Id = Guid.NewGuid(),
                Nome = "Rafael",
                Sobrenome = "Sano Katurabara",
                DataNascimento = DateTime.Now
            };
        }
    }
} 
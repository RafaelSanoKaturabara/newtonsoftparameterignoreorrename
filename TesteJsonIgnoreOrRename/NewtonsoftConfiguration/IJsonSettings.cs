﻿using Newtonsoft.Json;

namespace TesteJsonIgnoreOrRename.NewtonsoftConfiguration
{
    public interface IJsonSettings
    {
        void IgnoreProperties<T>(params string[] strParamToIgnoreArray) where T : class;

        void RenameProperties<T>(string propertyName, string newJsonPropertyName) where T : class;

        JsonSerializerSettings GetJsonSerializerSettings();
    }
}
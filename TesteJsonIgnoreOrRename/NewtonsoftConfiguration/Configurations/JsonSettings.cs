﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace TesteJsonIgnoreOrRename.NewtonsoftConfiguration.Configurations
{
    class JsonSettings : IJsonSettings
    {
        private JsonSerializerSettings _jsonSerializerSettings;
        private readonly PropertyRenameAndIgnoreSerializerContractResolver _jsonResolver;

        public JsonSettings()
        {
            _jsonSerializerSettings = new JsonSerializerSettings();
            _jsonResolver = new PropertyRenameAndIgnoreSerializerContractResolver();
        }

        public void IgnoreProperties<T>(params string[] strParamToIgnoreArray) where T : class
        {
            _jsonResolver.IgnoreProperty(typeof(T), strParamToIgnoreArray);
        }

        public void RenameProperties<T>(string propertyName, string newJsonPropertyName) where T : class
        {
            _jsonResolver.RenameProperty(typeof(T), propertyName, newJsonPropertyName);
        }

        public JsonSerializerSettings GetJsonSerializerSettings()
        {
            _jsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = _jsonResolver
            };
            return _jsonSerializerSettings;
        }
    }
}